const printReviewUrlMiddleware = (request, response, next) => {
    console.log("Request Review URL: ", request.url);

    next();
}

module.exports = { printReviewUrlMiddleware }